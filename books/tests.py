from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps

from .views import *
from .apps import *

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time

class UnitTest(TestCase):
    def test_app(self):
        self.assertEqual(BooksConfig.name, 'books')
        self.assertEqual(apps.get_app_config('books').name, 'books')

    def test_books_page_url_does_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_books_page_is_using_correct_function(self):
        found = resolve('/')
        self.assertEqual(found.func, books)

    def test_books_page_is_using_correct_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'books.html')

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(chrome_options = chrome_options)

    def tearDown(self):
        self.browser.quit()
        super().tearDown()

    def test_input(self):
        self.browser.get(self.live_server_url + '/')
        self.assertIn("Book Finder", self.browser.title)
        self.assertIn("Book Finder", self.browser.page_source)